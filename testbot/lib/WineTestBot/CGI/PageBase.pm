# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Base class for web pages
#
# Copyright 2009 Ge van Geldorp
# Copyright 2010 VMware, Inc.
# Copyright 2012-2014, 2017-2020, 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package WineTestBot::CGI::PageBase;

=head1 NAME

WineTestBot::CGI::PageBase - Common boilerplate for the WineTestBot web pages

This class defines the elements common to all the TestBot pages.

This includes services such as how to check the user session and how to
display errors. And it also defines the general look and feel of the page by
providing a common banner, navigation menu and cascading style sheets.

=cut

use Exporter 'import';
our @EXPORT = qw(CreatePageBase MakeSecureURL);

use Apache2::Const -compile => qw(REDIRECT);
use CGI::Cookie;
use URI::Escape;
use WineTestBot::CGI::Sessions;
use WineTestBot::Config;
use WineTestBot::Utils;


=pod
=over 12

=item C<new()>

Creates a PageBase object for the specified web page.

Parameters:
=over
=Page
This is the page that lead to the creation of this PageBase object.

=item Request
This is the HTTP request leading to this page.

=item RequiredRole
This is a role that the user must have in order to be allowed to access this
page (see UserRoles).

For instance an empty string specifies that any user can access the page,
even users with no account; while "admin" would specify that only logged-in
users with the "admin" role can access it.

=back

=back
=cut

sub new($$$$@)
{
  my $class = shift;
  my ($Page, $Request, $RequiredRole) = @_;

  my $self = {Request => $Request,
              Session => undef,
              Refresh => undef};
  $self = bless $self, $class;

  if (defined($RequiredRole) && $RequiredRole ne "")
  {
    $self->CheckSecurePage();
    my $Session = $self->GetCurrentSession();
    if (!defined $Session)
    {
      my $LoginURL = "/Login.pl?Target=" . uri_escape($ENV{"REQUEST_URI"});
      exit($self->Redirect(MakeSecureURL($LoginURL)));
    }
    elsif (!$Session->User->HasRole($RequiredRole))
    {
      exit($self->Redirect("/"));
    }
  }

  $self->_initialize(@_);
  return $self;
}

sub _initialize($$$$)
{
  #my ($self, $Page, $Request, $RequiredRole) = @_;
}

sub CreatePageBase($$$@)
{
  #my ($Page, $Request, $RequiredRole) = @_;
  return WineTestBot::CGI::PageBase->new(@_);
}

sub SetRefreshInterval($$)
{
  my ($self, $Seconds) = @_;

  $self->{Refresh} = $Seconds;
}


#
# Session management and security
#

=pod
=over 12

=item C<SecureConnection()>

Returns true if the user accessed the website over a secure connection.

This relies on the web server setting the $HTTPS environment variable for CGI
scripts.

=back
=cut

sub SecureConnection()
{
  return defined($ENV{"HTTPS"}) && $ENV{"HTTPS"} eq "on";
}

=pod
=over 12

=item C<MakeSecureURL()>

Builds a URL that accesses this website using https if possible.
The parameter should be an absolute path that includes neither the protocol
nor the hostname.

This relies on the web server setting the $HTTP_HOST environment variable for
CGI scripts. However $HTTP_HOST which may not match the official website
hostname. As such this should only be used for providing URLs back to the user
accessing the website, not for URLs sent to third-parties.

See also WineTestBot::Utils::MakeOfficialURL().

=back
=cut

sub MakeSecureURL($)
{
  my ($URL) = @_;

  my $Protocol = ($UseSSL || SecureConnection()) ? "https://" : "http://";
  return $Protocol . ($ENV{"HTTP_HOST"} || $WebHostName) . $URL;
}

sub UnsetCookies($)
{
  my ($self) = @_;

  my $Request = $self->{Request};
  my %Cookies = CGI::Cookie->fetch($Request);
  if (defined($Cookies{"SessionId"}))
  {
    my $Cookie = CGI::Cookie->new(-Name    => "SessionId",
                                  -Value   => "deleted",
                                  -Expires => "Sun, 25 Jul 1997 05:00:00 GMT",
                                  -Domain  => $ENV{"HTTP_HOST"},
                                  -Path    => "/",
                                  -SameSite => "Lax",
                                  -Secure  => $UseSSL);
    $Request->err_headers_out->add("Set-Cookie", $Cookie);
  }
  if (defined($Cookies{"SessionActive"}))
  {
    my $Cookie = CGI::Cookie->new(-Name    => "SessionActive",
                                  -Value   => "deleted",
                                  -Expires => "Sun, 25 Jul 1997 05:00:00 GMT",
                                  -Domain  => $ENV{"HTTP_HOST"},
                                  -Path    => "/",
                                  -SameSite => "Lax",
                                  -Secure  => !1);
    $Request->err_headers_out->add("Set-Cookie", $Cookie);
  }
  delete $Request->headers_in->{"Cookie"};
}

sub SetCookies($)
{
  my ($self) = @_;

  my $Request = $self->{Request};
  if ($self->SessionActive())
  {
    my $Session = $self->GetCurrentSession();
    my $Expire, my $SessionPermanent, my $Cookie;
    if (defined($Session))
    {
      if ($Session->Permanent)
      {
        $Expire = 2 ** 31 - 1;
        $SessionPermanent = "P";
      }
      else
      {
        $Expire = undef;
        $SessionPermanent = "T";
      }

      $Cookie = CGI::Cookie->new(-Name    => "SessionId",
                                 -Value   => $Session->Id,
                                 -Expires => $Expire,
                                 -SameSite => "Lax",
                                 -Secure  => $UseSSL,
                                 -HttpOnly => 1);
      $Request->err_headers_out->add("Set-Cookie", $Cookie);
    }
    else
    {
      my %Cookies = CGI::Cookie->fetch($Request);
      $SessionPermanent = $Cookies{"SessionActive"}->value;
      if ($SessionPermanent eq "P")
      {
        $Expire = 2 ** 31 - 1;
      }
      else
      {
        $Expire = undef;
      }
    }

    $Cookie = CGI::Cookie->new(-Name    => "SessionActive",
                               -Value   => $SessionPermanent,
                               -Expires => $Expire,
                               -SameSite => "Lax",
                               -Secure  => !1,
                               -HttpOnly => 1);
    $Request->err_headers_out->add("Set-Cookie", $Cookie);
  }
  else
  {
    $self->UnsetCookies();
  }
}

sub GetCurrentSession($)
{
  my ($self) = @_;

  if ($UseSSL && ! SecureConnection())
  {
    return undef;
  }

  if (! defined($self->{Session}))
  {
    my %Cookies = CGI::Cookie->fetch($self->{Request});
    if (defined($Cookies{"SessionId"}))
    {
      my $SessionId = $Cookies{"SessionId"}->value;
      my $Sessions = CreateSessions();
      $self->{Session} = $Sessions->GetItem($SessionId);
    }
  }

  return $self->{Session};
}

sub SetCurrentSession($$)
{
  my ($self, $Session) = @_;

  $self->{Session} = $Session;
  if (! defined($Session))
  {
    $self->UnsetCookies();
  }
}

sub SessionActive($)
{
  my ($self) = @_;

  if (defined($self->GetCurrentSession()))
  {
    return 1;
  }

  my %Cookies = CGI::Cookie->fetch($self->{Request});
  if ($UseSSL && ! SecureConnection() && defined($Cookies{"SessionActive"}))
  {
    return 1;
  }

  return !1;
}

sub Redirect($$)
{
  my ($self, $Location) = @_;

  $self->SetCookies();
  if (substr($Location, 0, 4) ne "http")
  {
    # Use the same protocol as for the current page. To force switching to
    # https the caller should use MakeSecureURL().
    my $Protocol = SecureConnection() ? "https://" : "http://";
    if (substr($Location, 0, 1) ne "/")
    {
      # Despite its name, Request->uri only contains the path portion of the
      # URI, excluding the protocol, hostname and parameters!
      my $URI = $self->{Request}->uri;
      $URI =~ s=^(.*)/[^/]*$=$1/$Location=;
      $Location = $URI;
    }
    $Location = $Protocol . $ENV{"HTTP_HOST"} . $Location;
  }
  $self->{Request}->headers_out->set("Location", $Location);
  $self->{Request}->status(Apache2::Const::REDIRECT);
  return 0; # a suitable exit code
}

sub CheckSecurePage($)
{
  my ($self) = @_;

  if ($UseSSL && ! SecureConnection())
  {
    exit($self->Redirect(MakeSecureURL($ENV{"REQUEST_URI"})));
  }
}


#
# Error handling framework
#

sub GenerateErrorDiv($$)
{
  my ($self, $Page) = @_;

  my $ErrMessage = $Page->GetErrMessage();
  if ($ErrMessage)
  {
    print "<noscript>\n";
    print "<div id='errormessage'>", $Page->escapeHTML($ErrMessage), "</div>\n";
    print "</noscript>\n";
  }
}

sub GenerateErrorPopup($$)
{
  my ($self, $Page) = @_;

  my $ErrMessage = $Page->GetErrMessage();
  if ($ErrMessage)
  {
    print "<script type='text/javascript'>\n";
    print "<!--\n";
    print "function ShowError() { alert(", $Page->JsQuote($ErrMessage), "); }\n";
    my $ErrField = $Page->GetFirstErrField();
    if ($ErrField)
    {
      print "document.forms[0].", $ErrField, ".focus();\n";
    }
    print "//-->\n";
    print "</script>\n";
  }
}


#
# HTML page generation
#

sub GetPageTitle($$)
{
  my ($self, $Page) = @_;

  my $Title = $Page->GetTitle() || "";
  $Title .= " - " if ($Title ne "");
  $Title .= "${ProjectName} Test Bot";
  return $Title;
}

sub GenerateImportJS($$)
{
  my ($self, $Filename) = @_;

  if (!$self->{js}->{$Filename})
  {
    print "<script type='text/javascript' src='$Filename'></script>\n";
    $self->{js}->{$Filename} = 1;
  }
}

sub GenerateHttpHeaders($)
{
  my ($self) = @_;

  my $Request = $self->{Request};

  # Date in the past
  $Request->headers_out->add("Expires", "Sun, 25 Jul 1997 05:00:00 GMT");

  # always modified
  $Request->headers_out->add("Last-Modified", (scalar gmtime) . " GMT");

  # HTTP/1.1
  $Request->headers_out->add("Cache-Control", "no-cache, must-revalidate, " .
                                              "post-check=0, pre-check=0");

  # HTTP/1.0
  $Request->headers_out->add("Pragma", "no-cache");

  # Force char set
  $Request->content_type("text/html; charset=UTF-8");

  $self->SetCookies();
}

sub GetOnLoadJavascriptFunction($$)
{
  my ($self, $Page) = @_;

  if ($Page->GetErrMessage())
  {
    return "ShowError()";
  }

  return undef;
}

sub GenerateHeader($$)
{
  my ($self, $Page) = @_;

  my $Title = $Page->escapeHTML($Page->GetPageTitle());
  print <<EOF;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>$Title</title>
  <link rel="shortcut icon" type="image/png" href="https://dl.winehq.org/share/images/winehq_logo_32.png">
  <link rel='stylesheet' href='/${ProjectName}TestBot.css' type='text/css' media='screen'>
</head>
EOF

  print "<body";
  my $OnLoadJavascriptFunction = $self->GetOnLoadJavascriptFunction($Page);
  if ($OnLoadJavascriptFunction)
  {
    print " onload='$OnLoadJavascriptFunction'";
  }
  print ">\n";

  print "<div id='logo_blurb'>${ProjectName} Test Bot</div>\n";
  if ($self->{Refresh})
  {
    print <<EOF;
<script type='text/javascript'>
<!--
var Reloader;
var Observer = new IntersectionObserver(function(entries) {
  if (Reloader)
  {
    clearTimeout(Reloader);
    Reloader = undefined;
  }
  if (entries[0].isIntersecting === true)
  {
    Reloader = setTimeout(function() { location.reload(); }, $self->{Refresh} * 1000);
  }
}, { threshold: [0] });

Observer.observe(document.querySelector("#logo_blurb"));
//-->
</script>
EOF
  }

  print <<EOF;
<div id='tabs'>
  <ul>
    <li><a href='//www.winehq.org/'>WineHQ</a></li>
    <li><a href='//wiki.winehq.org'>Wiki</a></li>
    <li><a href='//appdb.winehq.org/'>AppDB</a></li>
    <li><a href='//bugs.winehq.org/'>Bugzilla</a></li>
    <li class='s'><a href='//${WebHostName}/'>TestBot</a></li>
    <li><a href='//forum.winehq.org/'>Forums</a></li>
  </ul>
</div>

<div id="main_content">
  <div id="header">
    <div id="menu">
      <ul>
        <li class='top'><p>Test Bot</p></li>
        <li><p><a href='/'>Home</a></p></li>
EOF
  if (defined($PatchesMailingList))
  {
    print "        <li class='divider'>&nbsp;</li>\n";
    print "        <li><p><a href='/PatchesList.pl'>",
          ucfirst($PatchesMailingList), "</a></p></li>\n";
  }
  print "        <li><p><a href='/FailuresList.pl'>Failures</a></p></li>\n";

  my $Session = $self->GetCurrentSession();
  if ($self->SessionActive())
  {
    print "        <li class='divider'>&nbsp;</li>\n";
    print "        <li><p><a href='", MakeSecureURL("/Submit.pl"),
          "'>Submit job</a></p></li>\n";
    print "        <li><p><a href='/Activity.pl'>Activity</a></p></li>\n";
    print "        <li><p><a href='/Stats.pl'>Statistics</a></p></li>\n";
    print "        <li class='divider'>&nbsp;</li>\n";
    print "        <li><p><a href='", MakeSecureURL("/Logout.pl"), "'>Log out";
    if (defined($Session))
    {
      print " ", $Page->escapeHTML($Session->User->Name);
    }
    print "</a></p></li>\n";
  }
  else
  {
    if (! defined($LDAPServer))
    {
      print "        <li class='divider'>&nbsp;</li>\n";
      print "        <li><p><a href='/Register.pl'>Register</a></p></li>\n";
    }
    print "        <li class='divider'>&nbsp;</li>\n";
    print "        <li><p><a href='", MakeSecureURL("/Login.pl"),
          "'>Log in</a></p></li>\n";
  }
  print "        <li class='divider'>&nbsp;</li>\n";
  print "        <li><p><a href='/Feedback.pl'>Feedback</a></p></li>\n";
  print "        <li class='bot'>&nbsp;</li>\n";
  if (defined($Session) && $Session->User->HasRole("admin"))
  {
    print "        <li class='top'><p>Admin</p></li>\n";
    print "        <li><p><a href='", MakeSecureURL("/admin/SpecialJobs.pl"),
          "'>Special Jobs</a></p></li>\n";
    print "        <li><p><a href='", MakeSecureURL("/admin/UsersList.pl"),
          "'>Users</a></p></li>\n";
    print "        <li><p><a href='", MakeSecureURL("/admin/RolesList.pl"),
          "'>Roles</a></p></li>\n";
    print "        <li class='divider'>&nbsp;</li>\n";
    print "        <li><p><a href='", MakeSecureURL("/admin/VMsList.pl"),
          "'>VMs</a></p></li>\n";
    print "        <li class='divider'>&nbsp;</li>\n";
    print "        <li><p><a href='", MakeSecureURL("/admin/BranchesList.pl"),
          "'>Branches</a></p></li>\n";
    print "        <li class='divider'>&nbsp;</li>\n";
    print "        <li><p><a href='", MakeSecureURL("/admin/Log.pl"),
          "'>Engine Log</a></p></li>\n";
    print "        <li class='bot'>&nbsp;</li>\n";
  }

  print <<EOF;
      </ul>
    </div>
    <div id='banner'>
      <div id='Logo'>
        <a href='/'><img src='/${ProjectName}Logo.png' alt=''></a>
      </div>
      <div id='Project'>
        <img src='/${ProjectName}Project.png' alt=''>
      </div>
    </div>
  </div>

  <div class="rbox">
    <b class="rtop"><b class="r1">&nbsp;</b><b class="r2">&nbsp;</b><b class="r3">&nbsp;</b><b class="r4">&nbsp;</b></b>
    <div id="ContentContainer">
EOF
}

sub GenerateBody($)
{
  my ($self) = @_;

  die "Pure virtual function " . ref($self) . "::GenerateBody called";
}

sub GenerateFooter($)
{
  my ($self) = @_;

  print <<EOF;
    </div><!--ContentContainer-->
    <b id="PageEnd" class="rbottom"><b class="r4">&nbsp;</b><b class="r3">&nbsp;</b><b class="r2">&nbsp;</b><b class="r1">&nbsp;</b></b>
  </div>
</div><!--main_content-->

</body>
</html>
EOF
}

1;
