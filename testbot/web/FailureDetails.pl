# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Failure details page
#
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;


package TaskFailuresBlock;

use ObjectModel::CGI::CollectionBlock;
our @ISA = qw(ObjectModel::CGI::CollectionBlock);

use ObjectModel::BasicPropertyDescriptor;
use ObjectModel::CGI::ValueFormatter;

use WineTestBot::Jobs;
use WineTestBot::Utils;


sub GetPropertyDescriptors($)
{
  my ($self) = @_;

  # Add some extra columns
  my @PropertyDescriptors;
  foreach my $PropertyDescriptor (@{$self->{Collection}->GetPropertyDescriptors()})
  {
    my $PropertyName = $PropertyDescriptor->GetName();
    next if ($PropertyName =~ /^(?:JobId|StepNo|TaskNo)$/);
    push @PropertyDescriptors, $PropertyDescriptor;
    if ($PropertyName eq "TaskLog")
    {
      push @PropertyDescriptors,
           CreateBasicPropertyDescriptor("VM", "VM", !1, !1, "A", 20),
           CreateBasicPropertyDescriptor("Date", "Date", !1, !1, "DT", 19);
    }
  }
  push @PropertyDescriptors, CreateBasicPropertyDescriptor("Remarks", "Remarks", !1, !1, "A", 128);

  return \@PropertyDescriptors;
}

sub GenerateHeaderView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $PropertyName = $Col->{Descriptor}->GetName();
  if ($PropertyName eq "NewCount" or $PropertyName eq "OldCount")
  {
    $PropertyName =~ s/Count$//;
    print "<a class='title' title='", $self->escapeHTML($Col->{Descriptor}->GetDisplayName()), "'>$PropertyName</a>\n";
  }
  else
  {
    $self->SUPER::GenerateHeaderView($Row, $Col);
  }
}

sub GenerateDataView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $PropertyName = $Col->{Descriptor}->GetName();
  if ($PropertyName eq "Task")
  {
    my $JobId = $Row->{Item}->JobId;
    my $StepNo = $Row->{Item}->StepNo;
    my $TaskNo = $Row->{Item}->TaskNo;
    my $URL = GetTaskURL($JobId, $StepNo, $TaskNo);
    print "<a href='", $self->escapeHTML($URL), "'>$JobId/$StepNo/$TaskNo</a>";
  }
  elsif ($PropertyName eq "TaskLog")
  {
    my $URL = GetTaskURL($Row->{Item}->JobId, $Row->{Item}->StepNo,
                         $Row->{Item}->TaskNo, 0, $Row->{Item}->TaskLog);
    print "<a href='", $self->escapeHTML($URL), "'>",
          $self->escapeHTML($Row->{Item}->TaskLog), "</a>";
  }
  elsif ($PropertyName eq "VM")
  {
    print $self->escapeHTML($Row->{Item}->Task->VMName);
  }
  elsif ($PropertyName eq "Date")
  {
    # The enclosing page already pulled in the datetime JavaScript code
    GenerateDateTime($Row->{Item}->Task->Started);
  }
  elsif ($PropertyName eq "Remarks")
  {
    my $Job = CreateJobs()->GetItem($Row->{Item}->JobId);
    print $self->escapeHTML($Job->Remarks);
  }
  else
  {
    $self->SUPER::GenerateDataView($Row, $Col);
  }
}


package FailureDetailsPage;

use ObjectModel::CGI::ItemPage;
our @ISA = qw(ObjectModel::CGI::ItemPage);

use WineTestBot::Config;
use WineTestBot::Failures;


sub _initialize($$$)
{
  my ($self, $Request, $RequiredRole) = @_;

  $self->SUPER::_initialize($Request, $RequiredRole, CreateFailures());

  my $Session = $self->GetCurrentSession();
  if (!$Session or !$Session->User->HasRole("admin"))
  {
    # Only admin users can create and modify failures
    exit($self->RedirectToParent()) if ($self->{Item}->GetIsNew());
    $self->SetReadWrite(0);
  }
}

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $PropertyName = $PropertyDescriptor->GetName();
  return # Already shown in the title
         $PropertyName eq "Id" ? "" :
         # Showing these makes no sense for new items since they are read-only
         $PropertyName =~ /^(?:LastNew|LastOld|BugStatus|BugDescription)$/ ?
                       ($self->{Item}->GetIsNew() ? "" : "ro") :
         $self->SUPER::DisplayProperty($PropertyDescriptor);
}

sub GenerateValueView($$$$)
{
  my ($self, $PropertyDescriptor, $Value, $Format) = @_;

  if ($PropertyDescriptor->GetName() =~ /^Bug(?:Id|Status|Description)$/)
  {
    print "<a href='$WineBugUrl", $self->{Item}->BugId, "' target='_blank'>",
          $self->escapeHTML($Value || "Not fetched yet..."), "</a>";
  }
  else
  {
    $self->SUPER::GenerateValueView($PropertyDescriptor, $Value, $Format);
  }
}

sub GenerateBody($)
{
  my ($self) = @_;

  $self->SUPER::GenerateBody();
  if (!$self->{Item}->GetIsNew())
  {
    print "<h2><a name='TaskFailures'></a>Task Failures</h2>\n";
    my $TaskFailuresBlock = new TaskFailuresBlock($self->{Item}->TaskFailures, $self);
    $TaskFailuresBlock->SetReadWrite(0);
    $TaskFailuresBlock->GenerateList();
  }
}

sub GenerateFooter($)
{
  my ($self) = @_;
  print "<p></p><div class='CollectionBlock'><table>\n";
  print "<thead><tr><th class='Record'>Legend</th></tr></thead>\n";
  print "<tbody><tr><td>\n";

  print "<p>The <b>Test module</b> is in fact the name of the error group the failure may occur in. Most of the time this is a test module but it may also be the name of the group containing the extra errors.<br>\n";
  print "The <b>Test unit</b> is the name of the test unit the failure may occur in. For extra errors this should be an empty string.<br>\n";
  print "The <b>Configurations RegExp</b> defines which test configurations are known to exhibit this failure. It should match the configuration names which are of the form <i>VM:logfile</i> where <i>VM</i> is the name of the virtual machine (which for Windows VMs includes the test configuration); and <i>logfile</i> is a task log file, the most important of which are the test reports: <i>exe32</i> or <i>exe64</i> for Windows, and <i>win32</i>, <i>wow32</i> or <i>wow64</i> for Wine. On Wine the log filename may contain test configuration information, typically the locale, in an underscore separated suffix.<br>\n";
  print "The <b>Failures RegExp</b> should match the failure lines. Matching the test unit name is not necessary. Beware of the presence of wildcards in the error message such as: *+|()[]{}<br>\n";

  print "</td></tr></tbody>\n";
  print "</table></div>\n";
  $self->SUPER::GenerateFooter();
}

sub OnAction($$)
{
  my ($self, $Action) = @_;

  if ($Action eq "Save")
  {
    if (!$self->{RW})
    {
      $self->AddError("Only admin users can create or modify failures");
      return 0;
    }
    if (($self->{Item}->BugId || "") ne $self->GetParam("BugId"))
    {
      $self->{Item}->BugStatus("") if ($self->{Item}->BugStatus ne "deleted");
      $self->{Item}->BugDescription("");
    }
    return undef if (!$self->Save());
    exit($self->RedirectToParent());
  }
  return $self->SUPER::OnAction($Action);
}


package main;

my $Request = shift;
my $Page = FailureDetailsPage->new($Request, "");
$Page->GeneratePage();
