# Shows or sets the Windows locale settings
#
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

$Name0 = (Get-Item $MyInvocation.InvocationName).Basename


#
# Dump the Windows locales settings
#

$HKCU_INTERNATIONAL = "HKCU:\Control Panel\International"
$HKCU_GEO = "HKCU:\Control Panel\International\Geo"
$HKCU_USER_PROFILE = "HKCU:\Control Panel\International\User Profile"
$HKCU_DESKTOP = "HKCU:\Control Panel\Desktop"
$HKLM_LANGUAGE = "HKLM:\SYSTEM\CurrentControlSet\Control\Nls\Language"
$HKLM_LOCALE = "HKLM:\SYSTEM\CurrentControlSet\Control\Nls\Locale"
$HKLM_CODE_PAGE = "HKLM:\System\CurrentControlSet\Control\Nls\CodePage"
$CODE_PAGES = "ACP", "MACCP", "OEMCP"

$HKDEF_INTERNATIONAL = "Registry::HKEY_USERS\.DEFAULT\Control Panel\International"
$HKDEF_GEO = "Registry::HKEY_USERS\.DEFAULT\Control Panel\International\Geo"
$HKDEF_USER_PROFILE = "Registry::HKEY_USERS\.DEFAULT\Control Panel\International\User Profile"
$HKDEF_DESKTOP = "Registry::HKEY_USERS\.DEFAULT\Control Panel\Desktop"
$HKDEF_MUICACHED = "Registry::HKEY_USERS\.DEFAULT\Control Panel\Desktop\MuiCached"


function ShowSetting([string]$Key, [string]$VName, [string]$SName)
{
  if ($SName -eq "") { $SName = $VName }
  # Don't use Get-ItemPropertyValue because it ignores -ErrorAction and is
  # not available on Windows 7.
  $Value = Get-ItemProperty -Path $Key -Name $VName -ErrorAction SilentlyContinue
  $Value = if ($Value -eq $null) { "<undef>" } else { $Value.$($VName) }
  if ($Value.GetType().Name -eq "Object[]")
  {
    $Value = "[" + ($Value -join ", ") + "]"
  }
  Write-Output "$SName=$Value"
}

function ShowSettings()
{
  Write-Output "Current account:"
  ShowSetting $HKCU_INTERNATIONAL "Locale"
  ShowSetting $HKCU_INTERNATIONAL "LocaleName"
  ShowSetting $HKCU_GEO "Nation" "Country"
  ShowSetting $HKCU_GEO "Name" "CountryName"
  ShowSetting $HKCU_USER_PROFILE "InputMethodOverride" "InputMethod"
  ShowSetting $HKCU_USER_PROFILE "Languages"
  ShowSetting $HKCU_USER_PROFILE "WindowsOverride"
  ShowSetting $HKCU_DESKTOP "PreferredUILanguages"
  ShowSetting $HKCU_DESKTOP "PreferredUILanguagesPending"
  ShowSetting $HKCU_DESKTOP "PreviousPreferredUILanguages"

  Write-Output ""
  Write-Output "System settings:"
  ShowSetting $HKLM_LANGUAGE "Default" "SysLanguage"
  ShowSetting $HKLM_LANGUAGE "InstallLanguage" "SysInstallLang"
  try
  {
    ShowSetting $HKLM_LOCALE "(Default)" "SysLocale"
  }
  catch
  {
    # On Windows 7 Get-ItemProperty confuses the key's default value
    # with the value called "(Default)"!
    Write-Output "SysLocale=<cannot-get-on-windows-7>"
  }
  foreach ($CodePage in $CODE_PAGES)
  {
    ShowSetting $HKLM_CODE_PAGE $CodePage
  }

  Write-Output ""
  Write-Output ".DEFAULT account:"
  ShowSetting $HKDEF_INTERNATIONAL "Locale" "DefLocale"
  ShowSetting $HKDEF_INTERNATIONAL "LocaleName" "DefLocaleName"
  ShowSetting $HKDEF_GEO "Nation" "DefCountry"
  ShowSetting $HKDEF_GEO "Name" "DefCountryName"
  ShowSetting $HKDEF_USER_PROFILE "InputMethodOverride" "DefInputMethod"
  ShowSetting $HKDEF_USER_PROFILE "Languages" "DefLanguages"
  ShowSetting $HKDEF_USER_PROFILE "WindowsOverride" "DefWindowsOverride"
  ShowSetting $HKDEF_DESKTOP "PreferredUILanguages" "DefPreferredUILanguages"
  ShowSetting $HKDEF_DESKTOP "PreferredUILanguagesPending" "DefPreferredUILanguagesPending"
  ShowSetting $HKDEF_DESKTOP "PreviousPreferredUILanguages" "DefPreviousPreferredUILanguages"
  ShowSetting $HKDEF_MUICACHED "MachinePreferredUILanguages" "DefMachinePreferredUILanguages"

  exit 0
}

function GetLocaleInfo($Locale)
{
  $LInfo= New-WinUserLanguageList $Locale
  $LInfo = $LInfo[0]

  # Window synthesizes an Autonym for unknown/uninstalled locales (e.g. fr-DE),
  # but cannot recommend a keyboard layout.
  # Note also that installing a locale (e.g. fr-FR) typically also installs
  # (at least the core functionality of) related locales (e.g. fr-BE), even
  # though those are not shown as installed in the GUI.
  if ($LInfo.InputMethodTips[0] -eq $null)
  {
    return $null
  }

  # 'Canonicalize' the LanguageTag by picking the matching entry from the
  # installed languages list.
  $List = Get-WinUserLanguageList
  foreach ($L in $List)
  {
    if ($LInfo.Autonym -eq $L.Autonym)
    {
      return $L
    }
  }
  return $LInfo
}


function ShowLocaleInfo($Argv)
{
  $Locale = $Argv[1]
GetLocaleInfo($Locale)
  $LInfo = GetLocaleInfo($Locale)
  if ($LInfo -eq $null)
  {
    echo "$Name0:error: unknown locale $Locale. Is it installed?"
    exit 1
  }

  Write-Output "LanguageTag=$($LInfo.LanguageTag)"
  Write-Output "Autonym=$($LInfo.Autonym)"
  Write-Output "LocalizedName=$($LInfo.LocalizedName)"
  Write-Output "InputMethodTips=$($LInfo.InputMethodTips)"
  exit 0
}

#
# Modify the Windows locales settings through intl.cpl
#

function WriteIntlCplConfig([string]$Locale, [string]$CountryId, [string]$System, [string]$MUI, [string]$KeyboardId, [bool]$SysCopy, [bool]$DefCopy)
{
  # intl.cpl does not want single quotes on that first line!
  Write-Output '<gs:GlobalizationServices xmlns:gs="urn:longhornGlobalizationUnattend">'

  Write-Output "  <gs:UserList>"
  $CopyToSys = if ($SysCopy) { "true" } else { "false" }
  $CopyToDef = if ($DefCopy) { "true" } else { "false" }
  Write-Output "    <gs:User UserID='Current' CopySettingsToDefaultUserAcct='$CopyToDef' CopySettingsToSystemAcct='$CopyToSys'/>"
  Write-Output "  </gs:UserList>"

  if ($CountryId)
  {
    Write-Output "  <gs:LocationPreferences>"
    Write-Output "    <gs:GeoID Value='$CountryId'/>"
    Write-Output "  </gs:LocationPreferences>"
  }

  # Takes effect on the next log out + log in.
  if ($MUI)
  {
    # Note that specifying something like en-CA instead of en-GB fails here.
    # See the Set-WinUILanguageOverride comment above.
    Write-Output "  <gs:MUILanguagePreferences>"
    Write-Output "    <gs:MUILanguage Value='$MUI'/>"
    Write-Output "  </gs:MUILanguagePreferences>"
  }

  # Takes effect on the next reboot.
  if ($System)
  {
    Write-Output "  <gs:SystemLocale Name='$System'/>"
  }

  # Takes effect on the next log out + log in.
  if ($KeyboardId)
  {
    Write-Output "  <gs:InputPreferences>"
    Write-Output "    <gs:InputLanguageID Action='add' ID='$KeyboardId' Default='true'/>"
    Write-Output "  </gs:InputPreferences>"
  }

  if ($Locale)
  {
    Write-Output "  <gs:UserLocale>"
    Write-Output "    <gs:Locale Name='$Locale' SetAsCurrent='true' ResetAllSettings='true'>"
    Write-Output "    </gs:Locale>"
    Write-Output "  </gs:UserLocale>"
  }

  Write-Output "</gs:GlobalizationServices>"
}

function RunIntlCpl($XmlFilename)
{
  $IntlArg = 'intl.cpl,,/f:"' + $XmlFilename + '"'
  Write-Output "Running: control.exe $IntlArg"
  control.exe $IntlArg
  # intl.cpl executes asynchronously which means that:
  # - The exit code cannot be used to check for failures.
  # - The configuration file should not be removed too early.
  # - Further locale modifications (e.g. setting the code pages) should only
  #   be done after intl.cpl is done to avoid races.
  # So 'wait' for intl.cpl to be done by introducing an arbitrary pause.
  Start-Sleep 2
}


#
# Modify the Windows locales through Powershell
#

function SetCodePages($Value)
{
  foreach ($CodePage in $CODE_PAGES)
  {
    Set-ItemProperty -Path $HKLM_CODE_PAGE -Name $CodePage -Type String -Value $Value
  }
}

$SWC_Success = $True
$SWC_MUI = $null

function SetWinCulture([string]$Locale, [string]$CountryId, [string]$System, [string]$UTF8, [string]$MUI, [string]$KeyboardId, [bool]$SysCopy, [bool]$DefCopy)
{
  $Cmd = Get-Command Set-Culture -ErrorAction SilentlyContinue
  if ($Cmd -eq $null) {
    Write-Output "Set-Culture is not supported"
    $global:SWC_Success = $False
    return
  }

  if ($Locale) { Set-Culture $Locale }
  if ($CountryId) { Set-WinHomeLocation $CountryId }
  if ($System)
  {
    Set-WinSystemLocale $System
    $ACP = Get-ItemProperty -Path $HKLM_CODE_PAGE -Name "ACP" -ErrorAction SilentlyContinue
    if ($ACP.ACP -eq "65001")
    {
      Write-Output "$System is a UTF-8 system locale"
      if ($UTF8 -eq "no")
      {
        Write-Output "Setting the system locale to en-US instead"
        Set-WinSystemLocale "en-US"
      }
      $UTF8 = ""
    }
  }
  if ($UTF8 -eq "force") { SetCodePages(65001) }
  if ($MUI)
  {
    Set-WinUILanguageOverride $MUI
    $WinMUI = Get-ItemProperty -Path $HKCU_DESKTOP -Name "PreferredUILanguagesPending" -ErrorAction SilentlyContinue
    $global:SWC_MUI = $WinMUI.PreferredUILanguagesPending
    if ($SWC_MUI -ne $MUI)
    {
      Write-Output "Windows uses $SWC_MUI for the MUI language instead of $MUI"
    }
  }
  if ($KeyboardId) { Set-WinDefaultInputMethodOverride $KeyboardId }
  if ($SysCopy -or $DefCopy)
  {
    $Cmd = Get-Command Copy-UserInternationalSettingsToSystem -ErrorAction SilentlyContinue
    if ($Cmd -ne $null)
    {
      Copy-UserInternationalSettingsToSystem -WelcomeScreen $SysCopy -NewUser $DefCopy
    }
    else
    {
      Write-Output "Copy-UserInternationalSettingsToSystem is not supported. Falling back to intl.cpl for the locales copies."
      WriteIntlCplConfig $undef $undef $undef $undef $undef $SysCopy $DefCopy >"$Name0.xml"
      RunIntlCpl "$Name0.xml"
      Remove-Item -Path "$Name0.xml"
    }
  }
}


#
# The locale-change actions
#

function GetStringArg($Arg)
{
  # Treat '.' as equivalent to an empty string for convenience
  if ($Arg -ne ".") { $Arg } else { "" }
}

function ShowIntlConfig($Argv)
{
  $Locale = GetStringArg($Argv[1])
  $CountryId = GetStringArg($Argv[2])
  $System = GetStringArg($Argv[3])
  # The UTF8 parameter is irrelevant here
  $MUI = GetStringArg($Argv[5])
  $KeyboardId = GetStringArg($Argv[6])
  $SysCopy = $Argv[7] -ne "false"
  $DefCopy = $Argv[8] -ne "false"

  WriteIntlCplConfig $Locale $CountryId $System $MUI $KeyboardId $SysCopy $DefCopy
  exit 0
}

function SetLocales($Argv)
{
  $Locale = GetStringArg($Argv[1])
  $CountryId = GetStringArg($Argv[2])
  $System = GetStringArg($Argv[3])
  $UTF8 = GetStringArg($Argv[4])
  $MUI = GetStringArg($Argv[5])
  $KeyboardId = GetStringArg($Argv[6])
  $SysCopy = $Argv[7] -ne "false"
  $DefCopy = $Argv[8] -ne "false"
  $UseIntlCpl = $Argv[9] -eq "true"

  if ($UseIntlCpl -eq $False)
  {
    Write-OutPut "Setting the locales using the Powershell APIs"
    SetWinCulture $Locale $CountryId $System $UTF8 $MUI $KeyboardId $SysCopy $DefCopy
    if ($SWC_Success -eq $True) { exit 0 }
    Write-Output "Falling back to intl.cpl"
  }

  if ($SWC_MUI -eq $null) { $global:SWC_MUI = $MUI }
  WriteIntlCplConfig $Locale $CountryId $System $SWC_MUI $KeyboardId $SysCopy $DefCopy >"$Name0.xml"
  RunIntlCpl "$Name0.xml"
  Remove-Item -Path "$Name0.xml"

  if ($UTF8 -eq "force") { SetCodePages(65001) }
  exit 0
}


#
# Main
#

function ShowUsage()
{
  Write-Output "Usage: $Name0 settings"
  Write-Output "or     $Name0 info LOCALE"
  Write-Output "or     $Name0 intlconfig LOCALE COUNTRYID SYSTEM UTF8 MUI KEYBOARDID SYSCOPY DEFCOPY"
  Write-Output "or     $Name0 locales LOCALE COUNTRYID SYSTEM UTF8 MUI KEYBOARDID SYSCOPY DEFCOPY USEINTLCPL"
  Write-Output "or     $Name0 -?"
  Write-Output ""
  Write-Output "Shows or modifies the Windows locales."
  Write-Output ""
  Write-Output "Where:"
  Write-Output "  settings   Show the current Windows locale settings."
  Write-Output "  info       Show information about the specified locale. In particular it"
  Write-Output "             shows Windows' 'canonical' locale identifier."
  Write-Output "  intlconfig Generates an XML configuration file for intl.cpl."
  Write-Output "  locales    Modifies the locales either through the Powershell APIs or"
  Write-Output "             intl.cpl."
  Write-Output "  LOCALE     Is the BCP-47 locale to use for formats, date and time."
  Write-Output "  COUNTRYID  Is the numerical country code."
  Write-Output "  SYSTEM     Is the BCP-47 locale to use as the system locale."
  Write-Output "  UTF8       If set to 'force' the code pages will be changed to UTF-8"
  Write-Output "             regardless of the system locale."
  Write-Output "             If set to 'allow' the code pages will be set to match the system"
  Write-Output "             locale even if that implies using UTF-8. This is the default."
  Write-Output "             If set to 'no' Unicode-only locales will be replaced with en-US."
  Write-Output "  MUI        Is the BCP-47 locale to use for the display language."
  Write-Output "  KEYBOARDID Is the keyboard identifier to set as the default."
  Write-Output "  SYSCOPY    If 'true' the locale settings will be copied to the system accounts"
  Write-Output "             (such as used for the logon screen). This is the default."
  Write-Output "  DEFCOPY    If 'true' the locale settings will be copied to the default account"
  Write-Output "             (for new users). This is the default."
  Write-Output "  USEINTLCPL If 'true' apply the changes using intl.cpl instead of first trying"
  Write-Output "             the Powershell APIs."
  Write-Output "  -?         Shows this help message."
}

$Action = $args[0]
if ($Action -eq "settings") { ShowSettings }
if ($Action -eq "info") { ShowLocaleInfo $args }
if ($Action -eq "intlconfig") { ShowIntlConfig $args }
if ($Action -eq "locales") { SetLocales $args }
$Rc = 0
if ($Action -and $Action -ne "-?" -and $Action -ne "-h" -and $Action -ne "help")
{
  echo "$Name0:error: unknown action $Action"
  $Rc = 2
}
ShowUsage
exit $Rc
