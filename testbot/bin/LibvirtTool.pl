#!/usr/bin/perl -Tw
# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
#
# Performs poweroff, revert and other operations on the specified VM.
# These operations can take quite a bit of time, particularly in case of
# network trouble, and thus are best performed in a separate process.
#
# Copyright 2009 Ge van Geldorp
# Copyright 2012-2021 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

sub BEGIN
{
  if ($0 !~ m=^/=)
  {
    # Turn $0 into an absolute path so it can safely be used in @INC
    require Cwd;
    $0 = Cwd::cwd() . "/$0";
  }
  if ($0 =~ m=^(/.*)/[^/]+/[^/]+$=)
  {
    $::RootDir = $1;
    unshift @INC, "$::RootDir/lib";
  }
}
my $Name0 = $0;
$Name0 =~ s+^.*/++;

use WineTestBot::Config;
use WineTestBot::Log;
use WineTestBot::Utils;
use WineTestBot::VMs;

my $Debug;
sub Debug(@)
{
  print STDERR @_ if ($Debug);
}

my $LogOnly;
sub Error(@)
{
  print STDERR "$Name0:error: ", @_ if (!$LogOnly);
  LogMsg @_;
}


#
# Setup and command line processing
#

# Grab the command line options
my ($Usage, $Action, $VMKey);
while (@ARGV)
{
  my $Arg = shift @ARGV;
  if ($Arg eq "--debug")
  {
    $Debug = 1;
  }
  elsif ($Arg eq "--log-only")
  {
    $LogOnly = 1;
  }
  elsif ($Arg =~ /^(?:checkidle|checkoff|monitor|poweroff|revert)$/)
  {
    $Action = $Arg;
  }
  elsif ($Arg =~ /^(?:-\?|-h|--help)$/)
  {
    $Usage = 0;
    last;
  }
  elsif ($Arg =~ /^-/)
  {
    Error "unknown option '$Arg'\n";
    $Usage = 2;
    last;
  }
  elsif (!defined $VMKey)
  {
    $VMKey = $Arg;
  }
  else
  {
    Error "unexpected argument '$Arg'\n";
    $Usage = 2;
    last;
  }
}

# Check and untaint parameters
my $VM;
if (!defined $Usage)
{
  if (!defined $Action)
  {
    Error "you must specify the action to perform\n";
    $Usage = 2;
  }
  if (!defined $VMKey)
  {
    Error "you must specify the VM name\n";
    $Usage = 2;
  }
  else
  {
    my $UnTainted = WineTestBot::VM::UntaintName($VMKey);
    if (!defined $UnTainted)
    {
      Error "'$VMKey' is not a valid VM name\n";
      $Usage = 2;
    }
    else
    {
      $VMKey = $UnTainted;
      $VM = CreateVMs()->GetItem($VMKey);
      if (!defined $VM)
      {
        Error "The $VMKey VM does not exist\n";
        $Usage = 2;
      }
    }
  }
}
if (defined $Usage)
{
  print "Usage: $Name0 [--debug] [--log-only] [--help] (checkidle|checkoff|monitor|poweroff|revert) VMName\n";
  print "\n";
  print "Prepare the VM for use by the TestBot.\n";
  print "\n";
  print "Where:\n";
  print "  VMName     Is the name of the TestBot VM to work on.\n";
  print "  checkidle  Checks that the virtual machine is running the appropriate\n";
  print "             snapshot for the VM and sets the VM status to either idle (if it\n";
  print "             does), off (if it is powered off) or offline (in case of Libvirt\n";
  print "             error).\n";
  print "  checkoff   Checks that the virtual machine is powered off and sets the VM's\n";
  print "             status to either off (if it is) or offline (in case of Libvirt\n";
  print "             error).\n";
  print "  monitor    Tries accessing the virtual machine through Libvirt until success\n";
  print "             at which time the virtual machine is powered off and the VM\n";
  print "             status is set to off.\n";
  print "  poweroff   Powers off the virtual machine and sets the VM status to off.\n";
  print "  revert     Reverts and powers on the virtual machine to the snapshot\n";
  print "             specified by the VM. If the snapshot does not exist it may be\n";
  print "             created from an existing base snapshot by applying configuration\n";
  print "             options such as powering on the virtual machine, setting the\n";
  print "             locales, enabling test signing, etc.\n";
  print "  --debug    More verbose messages for debugging.\n";
  print "  --log-only Only send error messages to the log instead of also printing them\n";
  print "             on stderr.\n";
  print "  --help     Shows this usage message.\n";
  exit $Usage;
}


#
# Main
#

my $Start = Time();

my $CurrentStatus;

=pod
=over 12

=item C<FatalError()>

Logs the fatal error, notifies the administrator and exits the process.

This function never returns!

=back
=cut

sub FatalError($)
{
  my ($ErrMessage) = @_;
  Error $ErrMessage;

  # Get the up-to-date VM status
  $VM = CreateVMs()->GetItem($VMKey);

  if ($VM->Status eq "maintenance")
  {
    # Still proceed with changing the non-Status fields and notifying the
    # administrator to allow for error handling debugging.
  }
  elsif ($VM->Status ne $CurrentStatus)
  {
    LogMsg "Not updating the VM because its status changed: ". $VM->Status ." != $CurrentStatus\n";
    exit 1;
  }
  else
  {
    $VM->Status("offline");
  }
  $VM->ChildDeadline(undef);
  $VM->ChildPid(undef);
  my $Errors = $VM->AddError();

  my ($ErrProperty, $SaveErrMessage) = $VM->Save();
  if (defined $SaveErrMessage)
  {
    LogMsg "Could not put the $VMKey VM offline: $SaveErrMessage ($ErrProperty)\n";
  }
  elsif ($Errors < $MaxVMErrors)
  {
    NotifyAdministrator("Putting the $VMKey VM offline ($Errors)",
                        "Could not perform the $Action operation on the $VMKey VM:\n".
                        "\n$ErrMessage\n".
                        "Got $Errors consecutive errors.\n".
                        "The VM has been put offline and the TestBot will try to regain access to it.");
  }
  elsif ($Errors == $MaxVMErrors)
  {
    NotifyAdministrator("The $VMKey VM needs maintenance",
                        "Got $Errors consecutive errors working on the $VMKey VM:\n".
                        "\n$ErrMessage\n".
                        "It probably needs fixing to get back online.\n".
                        "* Spacing out attempts to revive it.\n".
                        "* No further error notification will be sent until it starts working again.\n");
  }
  exit 1;
}

=pod
=over 12

=item C<ChangeStatus()>

Checks that the VM status has not been tampered with and sets it to the new
value.

Returns a value suitable for the process exit code: 0 in case of success,
1 otherwise.

=back
=cut

sub ChangeStatus($$;$)
{
  my ($From, $To, $Done) = @_;

  # Get the up-to-date VM status
  $VM = CreateVMs()->GetItem($VMKey);
  if (!$VM or (defined $From and $VM->Status ne $From))
  {
    LogMsg "Not changing status\n";
    # Not changing the status is allowed in debug mode so the VM can be
    # put in 'maintenance' mode to avoid interference from the TestBot.
    return $Debug ? 0 : 1;
  }

  $VM->Status($To);
  if ($Done)
  {
    $VM->ChildDeadline(undef);
    $VM->ChildPid(undef);
  }
  my ($ErrProperty, $ErrMessage) = $VM->Save();
  if (defined $ErrMessage)
  {
    FatalError("Could not change the $VMKey VM status: $ErrMessage\n");
  }
  $CurrentStatus = $To;
  return 0;
}

=pod
=over 12

=item C<GetPrivilegedTA()>

Returns a connection to a TestAgent server with elevated privileges.

If the server running on the regular port does not have elevated privileges
this assumes that the one on the alternate port will. It is up to the caller
to report an error if that's not the case.

=back
=cut

sub GetPrivilegedTA($)
{
  my ($TA) = @_;

  # Use SetTime() to detect if privileged operations are allowed.
  return $TA->SetTime(0) ? $TA : $VM->GetAgent(1);
}

sub RunAndWait($$)
{
  my ($TA, $Cmd) = @_;
  Debug(Elapsed($Start), " Running: @$Cmd\n");
  my $Ret = $TA->RunAndWait($Cmd, 0, 30);
  Debug(Elapsed($Start), " $Cmd->[0] returned $Ret\n") if ($Ret);
  return $Ret;
}

sub Halt($$)
{
  my ($TA, $Reboot) = @_;

  my $Cmd = $VM->Type =~ /^win(?:32|64)$/ ?
            ["shutdown.exe", ($Reboot ? "/r" : "/s"), "/d", "00:00", "/t", "0"] :
            ["/sbin/shutdown", ($Reboot ? "--reboot" : "--poweroff"), "now"];
  Debug(Elapsed($Start), " Running: @$Cmd\n");
  my $PTA = GetPrivilegedTA($TA);
  if (!$PTA->Run($Cmd, 0))
  {
    Error "Could not run @$Cmd on $VMKey: ". $PTA->GetLastError() ."\n";
    return 0;
  }
  $TA->Disconnect();
  return 1;
}

=pod
=over 12

=item C<ShutDown()>

Attempt a clean shutdown of the VM (regardless of its current snapshot).

Returns 0 if the shutdown failed, 1 otherwise.
=back
=cut

sub ShutDown($$)
{
  my ($Domain, $TA) = @_;

  if (Halt($TA, 0))
  {
    Debug(Elapsed($Start), " Waiting for the VM to shut down\n");
    my $Deadline = time() + $WaitForShutdown;
    while (time() <= $Deadline)
    {
      return 1 if (!$Domain->IsPoweredOn());
      sleep(1);
    }
    Error "Timed out waiting for $VMKey to perform a clean shutdown\n";
  }
  return 0;
}

=pod
=over 12

=item C<ShutDownIfOffSnapshot()>

Attempt a clean shutdown of the VM if it is running a snapshot that requests
them (snapshot name containing '-off').

Returns 0 if the shutdown failed, 2 if it was successful and 1 if the VM was
already powered off or a shutdown was not needed.
=back
=cut
=back
=cut

sub ShutDownIfOffSnapshot()
{
  my $Domain = $VM->GetDomain();
  my $CurrentSnapshot = $Domain->GetSnapshotName();
  if (!defined $CurrentSnapshot or $CurrentSnapshot !~ /-off\b/ or
      !$Domain->IsPoweredOn())
  {
    return 1;
  }

  Debug(Elapsed($Start), " Performing a clean shutdown of $VMKey/$CurrentSnapshot\n");
  LogMsg "Performing a clean shutdown of $VMKey/$CurrentSnapshot\n";
  if (ShutDown($Domain, $VM->GetAgent()))
  {
    Debug(Elapsed($Start), " Successfully shut down $VMKey\n");
    LogMsg "Successfully shut down $VMKey\n";
    return 2;
  }

  # In fact the caller will do so
  Error "Forcefully shutting down $VMKey/$CurrentSnapshot now...\n";
  return 0;
}

sub Monitor()
{
  # Still try recovering the VM in case of repeated errors, but space out
  # attempts to not keep the host busy with a broken VM. Note that after
  # 1 hour the monitor process gets killed and replaced (to deal with stuck
  # monitor processes) but even so the VM will be checked once per hour.
  my $Interval = ($VM->Errors || 0) >= $MaxVMErrors ? 1860 : 60;
  my $NextTry = time() + $Interval;
  Debug(Elapsed($Start), " Checking $VMKey in ${Interval}s\n");

  $CurrentStatus = "offline";
  while (1)
  {
    # Get a fresh status
    $VM = CreateVMs()->GetItem($VMKey);
    if (!defined $VM or $VM->Role eq "retired" or $VM->Role eq "deleted" or
        $VM->Status eq "maintenance")
    {
      my $Reason = $VM ? "Role=". $VM->Role ."\nStatus=". $VM->Status :
                         "$VMKey does not exist anymore";
      NotifyAdministrator("The $VMKey VM is not relevant anymore",
                          "The $VMKey VM was offline but ceased to be relevant after ".
                          PrettyElapsed($Start). ":\n\n$Reason\n");
      return 1;
    }
    if ($VM->Status ne "offline")
    {
      NotifyAdministrator("The $VMKey VM is working again (". $VM->Status .")",
                          "The status of the $VMKey VM unexpectedly switched from offline\n".
                          "to ". $VM->Status ." after ". PrettyElapsed($Start) .".");
      return 0;
    }
    my $Sleep = $NextTry - time();
    if ($Sleep > 0)
    {
      # Check that the VM still needs monitoring at least once per minute.
      $Sleep = 60 if ($Sleep > 60);
      sleep($Sleep);
      next;
    }

    my $IsReady = $VM->GetDomain()->IsReady();
    if ($IsReady and $VM->GetDomain()->IsPoweredOn())
    {
      # Try to perform a clean shutdown if requested
      ShutDownIfOffSnapshot();

      my $ErrMessage = $VM->GetDomain()->PowerOff();
      if (defined $ErrMessage)
      {
        Error "$ErrMessage\n";
        $IsReady = undef;
      }
    }
    if ($IsReady)
    {
      # The VM may well still be broken, for instance if the required snapshot
      # does not exist. But only revert can tell. For this reason, let revert
      # announce when the VM works again.
      return 1 if (ChangeStatus("offline", "off", "done"));
      LogMsg("Switching $VMKey from offline to off after ". PrettyElapsed($Start) .".\n");
      return 0;
    }

    Debug(Elapsed($Start), " $VMKey is still busy / unreachable, trying again in ${Interval}s\n");
    $NextTry = time() + $Interval;
  }
}

sub PowerOff()
{
  # Try to perform a clean shutdown if requested
  ShutDownIfOffSnapshot();

  # Power off the VM no matter what its initial status is
  $CurrentStatus = $VM->Status;
  my $ErrMessage = $VM->GetDomain()->PowerOff();
  FatalError("$ErrMessage\n") if (defined $ErrMessage);

  return ChangeStatus(undef, "off", "done");
}

sub CheckIdle()
{
  $CurrentStatus = "dirty";
  my $IsPoweredOn = $VM->GetDomain()->IsPoweredOn();
  return ChangeStatus("dirty", "offline", "done") if (!defined $IsPoweredOn);
  return ChangeStatus("dirty", "off", "done") if (!$IsPoweredOn);

  my ($ErrMessage, $SnapshotName) = $VM->GetDomain()->GetSnapshotName();
  FatalError("$ErrMessage\n") if (defined $ErrMessage);

  # If the snapshot does not match then the virtual machine may be used by
  # another VM instance. So don't touch it. All that counts is that this
  # VM instance is not running.
  my $NewStatus = ($SnapshotName eq $VM->IdleSnapshot) ? "idle" : "off";
  return ChangeStatus("dirty", $NewStatus, "done");
}

sub CheckOff()
{
  $CurrentStatus = "dirty";
  my $IsPoweredOn = $VM->GetDomain()->IsPoweredOn();
  return ChangeStatus("dirty", "offline", "done") if (!defined $IsPoweredOn);

  if ($IsPoweredOn)
  {
    my ($ErrMessage, $SnapshotName) = $VM->GetDomain()->GetSnapshotName();
    FatalError("$ErrMessage\n") if (defined $ErrMessage);
    if ($SnapshotName eq $VM->IdleSnapshot)
    {
      # Try to perform a clean shutdown if requested
      ShutDownIfOffSnapshot();

      my $ErrMessage = $VM->GetDomain()->PowerOff();
      FatalError("$ErrMessage\n") if (defined $ErrMessage);
    }
  }

  return ChangeStatus("dirty", "off", "done");
}

sub WaitForBoot($)
{
  my ($TA) = @_;

  Debug(Elapsed($Start), " Waiting for the VM to boot\n");
  LogMsg "Waiting for $VMKey to boot\n";

  # Use small timeouts to avoid undue connection delays
  my ($OneTimeout, $MinAttempts, $MinTimeout) = $TA->SetConnectTimeout(5, 1, 5);
  my $Deadline = time() + $WaitForBoot;
  while (time() <= $Deadline)
  {
    my $Count = $TA->GetProperties("start.count");
    next if (!defined $Count); # Rebooting (TestAgentd is not running)
    last if ($Count != 0); # start.count == 1 after the reboot
    sleep(10); # Not rebooting yet
  }
  $TA->SetConnectTimeout($OneTimeout, $MinAttempts, $MinTimeout);
}

sub WaitForSession($)
{
  my ($TA) = @_;

  if ($VM->Type eq "wine")
  {
    # For Wine VMs we have to wait some more for the X session to start.
    Debug(Elapsed($Start), " Waiting for the X session\n");
    LogMsg "Waiting for the $VMKey X session\n";
    my $Pid = $TA->Run(["sh", "-c", "while ! xset -display :0.0 q >/dev/null; do sleep 1; done"], 0);
    FatalError("Could not check for the $VMKey X session\n") if (!$Pid);

    if (!defined $TA->Wait($Pid, $SleepAfterBoot))
    {
      my $ErrMessage = $TA->GetLastError();
      if ($ErrMessage =~ /timed out waiting for the child process/)
      {
        FatalError("Timed out waiting for the $VMKey X session\n");
      }
      FatalError("An error occurred while waiting for the $VMKey X session: $ErrMessage\n");
    }
  }
}

sub CheckBootCount($)
{
  my ($TA) = @_;

  # Check that TestAgentd is not displaying the "Has Windows rebooted?" warning.
  my $Count = $TA->GetProperties("start.count");
  if (defined $Count and $Count > 1)
  {
    FatalError("$VMKey has been rebooted too many times: start.count=$Count > 1\n");
  }
}

sub ResetBootCount($)
{
  my ($TA) = @_;

  # Check the boot count before resetting it
  CheckBootCount($TA);
  $TA->SetProperty("start.count", 0);
}

sub PrepareVM($)
{
  my ($TA) = @_;

  Debug(Elapsed($Start), " Setting up the $VMKey TestAgent server\n");
  LogMsg "Setting up the $VMKey TestAgent server\n";
  my $Version = $TA->GetVersion();
  if (!$Version)
  {
    my $ErrMessage = $TA->GetLastError();
    FatalError("Could not connect to the $VMKey TestAgent: $ErrMessage\n");
  }

  # Always update the guest's system time
  # This is needed not only for the tests (date is important when checking
  # HTTPS certificates), but also for the build (if the time moves forward
  # during the build some ccache versions will return a compilation error).
  my $PTA = GetPrivilegedTA($TA);
  FatalError("$VMKey has no privileged TestAgent server\n") if (!$PTA);
  if (!$PTA->SetTime(0))
  {
    FatalError("Setting the $VMKey system time failed: ". $PTA->GetLastError() ."\n");
  }
}

sub GetSnapshotConfig($$)
{
  my ($Domain, $Config) = @_;

  # Notes:
  # - Don't use a live snapshot as a base snapshot:
  #   - It's probably better if snapshots are independent from each other.
  #   - Applying extra options on top of a live snapshot may require a reboot.
  #     But some locales changes may not survive that. So using, for instance,
  #     test-fr-FR-live as a base may break the locale.
  # - However some options are not supported on all Windows versions. For
  #   instance 'adm' snapshots cannot be created on Windows 7 so in that case
  #   it is useful to be able to use a preconfigured test-adm as a base
  #   snapshot for test-adm-live.
  # - Finally, although base snapshots, such as 'test', are presumably powered
  #   off, 'off' snapshots still create a new snapshot. The difference is that
  #   the 'test-off' snapshot will have an up-to-date TestAgentd server, the
  #   right desktop background, etc.
  while (1)
  {
    if ($Config->{base} !~ /-live\b/ and
        $Domain->HasSnapshot($Config->{base}))
    {
      last;
    }
    if ($Config->{base} =~ s/-([a-z]{2}-[A-Z]{2})$//)
    {
      $Config->{locale} ||= $1; # use the last locale in the snapshot name
    }
    elsif ($Config->{base} =~ s/-(adm|live|off|tsign|u8)$//)
    {
      $Config->{$1} = 1;
    }
    else
    {
      last;
    }
  }
}

sub CreateSnapshot($$$$)
{
  my ($Domain, $TA, $Config, $Booting) = @_;

  my $Reboot;
  if ($VM->Type =~ /^win(?:32|64)$/)
  {
    my $Version = $TA->GetVersion();
    Debug(Elapsed($Start), " Upgrading the $VMKey TestAgent server from $Version\n");
    LogMsg "Upgrading the $VMKey TestAgent server from $Version\n";
    if ($Version !~ / ([0-9]+)\.([0-9]+)$/)
    {
      FatalError("Unsupported TestAgent server version on $VMKey: $Version\n");
    }
    # We want 'TestAgentd --detach --show-restarts' on Windows but this was
    # not supported before this version and changing how the server is started
    # is too complex.
    if (!$TA->HasMinVersion(1, 7))
    {
      FatalError("The $VMKey TestAgent server is too old to be upgraded: $Version\n");
    }

    my $Bits = ($VM->Type =~ /64/) ? "64" : "32";
    if (!$TA->Upgrade("$DataDir/latest/TestAgentd$Bits.exe"))
    {
      my $ErrMessage = $TA->GetLastError();
      FatalError("Could not upgrade the $VMKey TestAgent: $ErrMessage\n");
    }
    # Give the server enough time to restart, thus (maybe) avoiding a timeout
    # on the first (re)connection attempt.
    sleep(1);
    $Version = $TA->GetVersion();
    if (!$Version)
    {
      my $ErrMessage = $TA->GetLastError();
      FatalError("Could not connect to the new $VMKey TestAgent: $ErrMessage\n");
    }
    LogMsg "Upgraded the $VMKey TestAgent server to $Version\n";

    # Note that the privileged TestAgent server (if any) is usually run with
    # --set-time-only which means it cannot be upgraded since the restart RPC
    # is blacklisted. But that also means it's unlikely to need upgrading.
    # A side effect is that it will force TestAgentd.exe.old to stay around.

    # Remove the Windows desktop wallpaper so we get smaller screenshots.
    # On Vista+ these require a logout+login to take effect.
    if (RunAndWait($TA, ["reg.exe", "delete", "HKEY_CURRENT_USER\\Control Panel\\Desktop", "/v", "Wallpaper", "/f"]))
    {
      FatalError("Could not reset the $VMKey Windows desktop wallpaper\n");
    }
    if (RunAndWait($TA, ["reg.exe", "add", "HKEY_CURRENT_USER\\Control Panel\\Colors", "/v", "Background", "/t", "REG_SZ", "/d", "140 20 20", "/f"]))
    {
      FatalError("Could not reset the $VMKey Windows desktop background color\n");
    }

    # Reduce the boot delay. The way to do this depends on the Windows
    # version so this only works on recent ones :-(
    my $PTA = GetPrivilegedTA($TA);
    if (RunAndWait($PTA, ["bcdedit.exe", "/set", "{bootmgr}", "timeout", "1"]))
    {
      Error("Could not change the Windows boot delay on $VMKey\n");
    }

    # Stop and disable the Windows time service to be sure it won't change
    # the time while a test is running.
    if (RunAndWait($PTA, ["net.exe", "stop", "w32time"]))
    {
      Error("Could not stop the $VMKey Windows time service\n");
    }
    if (RunAndWait($PTA, ["sc.exe", "config", "w32time", "start=", "disabled"]))
    {
      Error("Could not disable the $VMKey Windows time service\n");
    }
    if (RunAndWait($TA, ["reg.exe", "add", "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\W32Time\\Parameters", "/v", "Type", "/t", "REG_SZ", "/d", "NoSync", "/f"]))
    {
      FatalError("Could not set the $VMKey Windows Time service to NoSync\n");
    }

    if ($Config->{tsign})
    {
      # Takes effect after the next reboot
      if (RunAndWait($PTA, ["bcdedit.exe", "/set", "testsigning", "on"]))
      {
        FatalError("Could not turn on test signing on $VMKey\n");
      }
    }

    if ($Config->{adm})
    {
      # Takes effect after the next reboot
      my $PS1 = "$0.ps1";
      $PS1 =~ s/\.pl//;
      if (!$TA->SendFile($PS1, "$Name0.ps1", 0))
      {
        FatalError("Could not send '$Name0.ps1'\n");
      }
      if (RunAndWait($PTA, ["powershell.exe", "-ExecutionPolicy", "ByPass",
                            "-File", "$Name0.ps1", "admin", $AgentPort]))
      {
        FatalError("Could not set up the TestAgentd tasks on $VMKey\n");
      }
      $PTA->Rm("$Name0.ps1");
    }

    $Reboot = 1;
  }

  my @Locale;
  push @Locale, $Config->{locale} if ($Config->{locale});
  push @Locale, $Config->{u8} if ($Config->{u8});
  if (@Locale)
  {
    Debug(Elapsed($Start), " Setting up the @Locale locale on $VMKey\n");
    # Use the --vm option to make sure SetWinLocale uses the right TestAgent
    # connection parameters: port, ssh tunneling, etc.
    my @Cmd = ("$BinDir/SetWinLocale", "--vm", $VMKey);
    if ($Config->{locale})
    {
      push @Cmd, "--default", $Config->{locale};
      # SetWinLocale --default already performs a reboot and WaitForBoot()
      ResetBootCount($TA);
      $Reboot = undef;
      $Booting = 1;
    }
    push @Cmd, "--utf8" if ($Config->{u8});
    push @Cmd, "--debug" if ($Debug);
    $TA->Disconnect();

    Debug("Running: @Cmd\n");
    if (system(@Cmd))
    {
      FatalError("Could not set the $VMKey locale to @Locale\n");
    }
  }

  if ($Config->{live})
  {
    if ($Reboot)
    {
      # A reboot is still required for some changes to take effect
      ResetBootCount($TA);
      Halt($TA, "reboot");
      WaitForBoot($TA);
      $Booting = 1;
    }
    if ($Booting)
    {
      WaitForSession($TA);
      if ($SleepAfterBoot)
      {
        Debug(Elapsed($Start), " Sleeping ${SleepAfterBoot}s for the $Config->{snapshot} snapshot\n");
        LogMsg "Letting $VMKey settle down for the $Config->{snapshot} snapshot\n";
        sleep($SleepAfterBoot);
      }
    }
    CheckBootCount($TA);
    $TA->Disconnect(); # disconnect before taking the snapshot
  }
  else
  {
    # Power off the VM for the snapshot
    Debug(Elapsed($Start), " Shutting down $VMKey to take the $Config->{snapshot} snapshot\n");
    ResetBootCount($TA);
    ShutDown($Domain, $TA);
  }

  Debug(Elapsed($Start), " Creating the $Config->{snapshot} snapshot\n");
  my $ErrMessage = $Domain->CreateSnapshot($Config->{snapshot});
  if (defined $ErrMessage)
  {
    FatalError("Could not create the $Config->{snapshot} snapshot on $VMKey: $ErrMessage\n");
  }
  return 0 if ($Config->{live});

  # Without this small pause Libvirt/QEmu sometimes forgets to power on the VM!
  sleep(1);

  # The VM was powered off for the snapshot but we need it to be running for
  # Revert(). So (revert &) power it on again.
  Debug(Elapsed($Start), " Starting $VMKey from the new $Config->{snapshot} snapshot\n");
  ($ErrMessage, $Booting) = $Domain->RevertToSnapshot($Config->{snapshot});
  if (defined $ErrMessage)
  {
    FatalError("Could not revert $VMKey to $Config->{snapshot}: $ErrMessage\n");
  }
  WaitForBoot($TA);
  return 1;
}

sub Revert()
{
  if (!$Debug and $VM->Status ne "reverting")
  {
    Error("$VMKey is not ready to be reverted (". $VM->Status .")\n");
    return 1;
  }
  $CurrentStatus = "reverting";
  my $Config = {
    base => $VM->IdleSnapshot,
    snapshot => $VM->IdleSnapshot,
  };

  my $Domain = $VM->GetDomain();
  if (!$Domain->HasSnapshot($Config->{base}))
  {
    $Config->{create} = 1;
    GetSnapshotConfig($Domain, $Config);
    Debug(join(" ", Elapsed($Start), "Config:", map { "$_=$Config->{$_}" } sort keys %$Config), "\n");
    if ($Config->{locale} and $VM->Type !~ /^win(?:32|64)$/)
    {
      FatalError("Creating locale snapshots ($Config->{locale}) is not supported for ". $VM->Type ." VMs ($VMKey)\n");
    }
    if ($Config->{locale} and !$Config->{live})
    {
      # The --locale and --country changes don't persist across reboots :-(
      FatalError("Creating non-live locale snapshots is not supported ($VMKey)\n");
    }
    if (!$Domain->HasSnapshot($Config->{base}))
    {
      FatalError("Could not find $VMKey\'s $Config->{base} snapshot\n");
    }

    # Extend the deadline to have time to set up the VM and (re)boot it.
    # Add $WaitForBoot even if no reboot is needed in the end.
    my $ExtraTimeout = $WaitForBoot + $VMToolTimeout;
    Debug(Elapsed($Start), " Extending the $VMKey revert deadline by ${ExtraTimeout}s\n");
    # ChildDeadline is undefined if the VM (or another sharing the same
    # hypervisor domain) is in maintenance mode.
    my $Deadline = $VM->ChildDeadline || (time() + $VMToolTimeout);
    $VM->ChildDeadline($Deadline + $ExtraTimeout);
    $VM->Save();
  }

  # Before reverting, try to perform a clean shutdown if requested.
  # After a shutdown a small wait is also sometimes required, at least when
  # switching from a PCI-passthrough configuration to a regular one.
  sleep(1) if (ShutDownIfOffSnapshot() == 2);

  # Revert the VM (and power it on if necessary)
  Debug(Elapsed($Start), " Reverting $VMKey to $Config->{base}\n");
  my ($ErrMessage, $Booting) = $Domain->RevertToSnapshot($Config->{base});
  if (defined $ErrMessage)
  {
    # Libvirt/QEmu is buggy and cannot revert a running VM from one hardware
    # configuration to another. So try again after forcefully powering off
    # the VM, though this can be much slower.
    Debug(Elapsed($Start), " Powering off the VM\n");
    $ErrMessage = $Domain->PowerOff();
    if (defined $ErrMessage)
    {
      FatalError("Could not power off $VMKey: $ErrMessage\n");
    }

    Debug(Elapsed($Start), " Reverting $VMKey to $Config->{base}... again\n");
    ($ErrMessage, $Booting) = $Domain->RevertToSnapshot($Config->{base});
  }
  if (defined $ErrMessage)
  {
    FatalError("Could not revert $VMKey to $Config->{base}: $ErrMessage\n");
  }

  if (!$Config->{create})
  {
    # Mark the VM as sleeping which allows the scheduler to abort the revert in
    # favor of higher priority tasks. But don't allow interruptions in the
    # middle of snapshot creation!
    return 1 if (ChangeStatus("reverting", "sleeping"));
  }

  my $TA = $VM->GetAgent();
  WaitForBoot($TA) if ($Booting);
  PrepareVM($TA);

  if ($Config->{create})
  {
    $Booting = CreateSnapshot($Domain, $TA, $Config, $Booting);

    if ($VM->Type eq "build" or $VM->Type eq "wine")
    {
      require WineTestBot::SpecialJobs;
      $ErrMessage = WineTestBot::SpecialJobs::AddReconfigJob([$VM], $VM->Name, $VM->Type);
      if (defined $ErrMessage)
      {
        Error("Could not create a job to update and rebuild Wine on the $VMKey VM: $ErrMessage\n");
        NotifyAdministrator("Could not create a job to update $VMKey",
          "A live snapshot was created for $VMKey but no job could be\n".
          "created to update and rebuild Wine on it:\n\n".
          "$ErrMessage\n");
      }
      else
      {
        Debug(Elapsed($Start), " Added a job to update and rebuild Wine on $VMKey\n");
      }
    }

    # The activity monitor does not like it when VMs skip the sleeping step
    return 1 if (ChangeStatus("reverting", "sleeping"));
  }

  WaitForSession($TA) if ($Booting);

  my $Sleep = ($Booting and $SleepAfterBoot > $SleepAfterRevert) ?
              $SleepAfterBoot : $SleepAfterRevert;
  Debug(Elapsed($Start), " Sleeping ${Sleep}s\n");
  LogMsg "Letting $VMKey settle down for ${Sleep}s\n";
  sleep($Sleep);
  CheckBootCount($TA);

  return 1 if (ChangeStatus($CurrentStatus, "idle", "done"));
  if ($VM->Errors)
  {
    NotifyAdministrator("The $VMKey VM is working again",
                        "After ". $VM->Errors ." consecutive errors $VMKey was successfully reverted to $Config->{snapshot} in ". PrettyElapsed($Start) ." and is now idle.");
  }
  return 0;
}


my $Rc;
if ($Action eq "checkidle")
{
  $Rc = CheckIdle();
}
elsif ($Action eq "checkoff")
{
  $Rc = CheckOff();
}
elsif ($Action eq "monitor")
{
  $Rc = Monitor();
}
elsif ($Action eq "poweroff")
{
  $Rc = PowerOff();
}
elsif ($Action eq "revert")
{
  $Rc = Revert();
}
else
{
  Error("Unsupported action $Action!\n");
  $Rc = 1;
}
LogMsg "$Action on $VMKey completed in ", PrettyElapsed($Start), "\n";

exit $Rc;
