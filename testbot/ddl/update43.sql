USE winetestbot;

ALTER TABLE PendingPatches
  DROP FOREIGN KEY PendingPatches_ibfk_1;


ALTER TABLE PendingPatchSets
  ADD Version INT(2) NOT NULL
      AFTER Email;

ALTER TABLE PendingPatchSets
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (EMail, Version, TotalParts);


ALTER TABLE PendingPatches
  ADD PendingPatchSetVersion INT(2) NOT NULL
      AFTER PendingPatchSetEmail;

ALTER TABLE PendingPatches
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (PendingPatchSetEMail, PendingPatchSetVersion, PendingPatchSetTotalParts, No);

ALTER TABLE PendingPatches
  ADD FOREIGN KEY (PendingPatchSetEMail, PendingPatchSetVersion, PendingPatchSetTotalParts)
      REFERENCES PendingPatchSets(EMail, Version, TotalParts);
